from unittest import TestCase

from main.messageTypes.bounceOrForward import BounceOrForward
from tests.domain.test_woman import create_empty_proposal, create_accepted_proposal, create_rejected_proposal, \
    create_accepted_rejected_proposal
from main.proposeManager import ProposeManager
from main.domain.woman import Woman
from main.domain.man import Man


class TestProposeManager(TestCase):

    def test_choose_least_desirable(self):
        test_woman = Woman("test_woman")
        test_woman.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 2})
        test_woman.accepted_proposals = [create_accepted_proposal("a_1", test_woman.get_name()),
                                         create_accepted_proposal("a_2", test_woman.get_name()),
                                         create_accepted_proposal("a_3", test_woman.get_name())]

        propose_manager = ProposeManager([Man("a_1"), Man("a_2"), Man("a_3"), Man("a_4")], [test_woman])

        response_from_woman = BounceOrForward(test_woman.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman.get_name()), test_woman.name)

        list_of_proposals = propose_manager.find_least_desirable_proposal_from_list_for_given_woman(response_from_woman,
                                                                                                    test_woman.name)

        self.assertTrue(len(list_of_proposals) == 1)
        self.assertEqual(list_of_proposals[0], create_accepted_proposal("a_3", test_woman.get_name()))

    def test_choose_least_desirable_with_their_ranks(self):
        test_woman = Woman("test_woman")
        test_woman.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman.accepted_proposals = [create_accepted_proposal("a_1", test_woman.get_name()),
                                         create_accepted_proposal("a_2", test_woman.get_name()),
                                         create_accepted_proposal("a_3", test_woman.get_name())]

        a_1 = Man("a_1")
        a_2 = Man("a_2")
        a_3 = Man("a_3")
        a_4 = Man("a_4")
        a_4.promotion_level = 2
        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman])

        response_from_woman = BounceOrForward(test_woman.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman.get_name()), test_woman.name)

        list_of_proposals = propose_manager.find_least_desirable_proposal_from_list_for_given_woman(response_from_woman,
                                                                                                    test_woman.name)

        self.assertTrue(len(list_of_proposals) == 1)
        self.assertEqual(list_of_proposals[0], create_empty_proposal("a_4", test_woman.get_name()))

    def test_bounce_proposal_successful(self):
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_1": 1})
        test_woman_3.accepted_proposals = [create_accepted_proposal("a_2", test_woman_3.get_name()),
                                           create_accepted_proposal("a_2", test_woman_3.get_name()),
                                           create_accepted_proposal("a_3", test_woman_3.get_name())]

        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1, "test_woman_3": 1})
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1})

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3])

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()), test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 3)
        self.assertIn(create_accepted_rejected_proposal("a_1", test_woman_1.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_1", test_woman_2.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_4", test_woman_1.name), list_of_proposal)

    def test_bounce_proposal_successful_1(self):
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})
        test_woman_2.accepted_proposals = [create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_3", test_woman_2.get_name())]

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_1": 1})

        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1, "test_woman_3": 1})
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1})

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3])

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()), test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 3)
        self.assertIn(create_accepted_rejected_proposal("a_1", test_woman_1.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_1", test_woman_3.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_4", test_woman_1.name), list_of_proposal)

    def test_bounce_proposal_is_unsuccessful(self):
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})
        test_woman_2.accepted_proposals = [create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_3", test_woman_2.get_name())]

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_1": 1})
        test_woman_3.accepted_proposals = [create_accepted_proposal("a_2", test_woman_3.get_name()),
                                           create_accepted_proposal("a_2", test_woman_3.get_name()),
                                           create_accepted_proposal("a_3", test_woman_3.get_name())]

        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1, "test_woman_3": 1})
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1})

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3])

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()), test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 0)

    def test_bounce_proposal_is_unsuccessful_2(self):
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})
        test_woman_2.accepted_proposals = [create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_3", test_woman_2.get_name())]

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_2": 1})

        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1, })
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1})

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3])

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()),
                                              test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 0)

    def test_bounce_proposal_is_successful_3(self):
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_2", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})
        test_woman_2.accepted_proposals = [create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_3", test_woman_2.get_name())]

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_2": 1})

        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1, })
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1, "test_woman_3": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1})

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3])

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()),
                                              test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 3)
        self.assertIn(create_accepted_rejected_proposal("a_2", test_woman_1.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_2", test_woman_3.name), list_of_proposal)
        self.assertIn(create_accepted_proposal("a_4", test_woman_1.name), list_of_proposal)

    def test_bounce_proposal_is_successful_4(self):
        # Women
        test_woman_1 = Woman("test_woman_1")
        test_woman_1.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 1})
        test_woman_1.accepted_proposals = [create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_1", test_woman_1.get_name()),
                                           create_accepted_proposal("a_2", test_woman_1.get_name())]

        test_woman_2 = Woman("test_woman_2")
        test_woman_2.set_preference_dict({"a_1": 1, "a_2": 1, "a_3": 1, "a_4": 2})
        test_woman_2.accepted_proposals = [create_accepted_proposal("a_2", test_woman_2.get_name()),
                                           create_accepted_proposal("a_3", test_woman_2.get_name())]

        test_woman_3 = Woman("test_woman_3")
        test_woman_3.set_preference_dict({"a_2": 1})

        test_woman_4 = Woman("test_woman_4")
        test_woman_4.set_preference_dict({"a_3": 2, "a_4": 1})

        # Men
        a_1 = Man("a_1")
        a_1.set_preference_dict({"test_woman_1": 1})
        a_2 = Man("a_2")
        a_2.set_preference_dict({"test_woman_1": 1})
        a_3 = Man("a_3")
        a_3.set_preference_dict({"test_woman_1": 1})
        a_4 = Man("a_4")
        a_4.set_preference_dict({"test_woman_1": 1, "test_woman_2": 1})

        # Propose Manager

        propose_manager = ProposeManager([a_1, a_2, a_3, a_4], [test_woman_1, test_woman_2, test_woman_3, test_woman_4])

        # Response from woman

        response_from_woman = BounceOrForward(test_woman_1.accepted_proposals,
                                              create_empty_proposal("a_4", test_woman_1.get_name()),
                                              test_woman_1.name)

        list_of_proposal = propose_manager.bounce_proposal(response_from_woman, test_woman_1)

        self.assertTrue(len(list_of_proposal) == 1)
        self.assertIn(create_accepted_proposal("a_4", test_woman_2.name), list_of_proposal)
