from unittest import TestCase
from main.domain.woman import Woman
from main.messageTypes.proposal import Proposal
from main.messageTypes.bounceOrForward import BounceOrForward


class TestWoman(TestCase):
    def test_receives_proposal_and_returns_proposal_with_response(self):
        test_woman = Woman("test_woman")
        test_woman.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 2})
        test_woman.accepted_proposals = [create_accepted_proposal("a_1", test_woman.name),
                                         create_accepted_proposal("a_2", test_woman.name)]

        proposal = Proposal("a_3", test_woman.name)

        response = test_woman.receives_proposal_and_responds(proposal)

        self.assertTrue(isinstance(response, Proposal))
        self.assertEqual(response.get_made_by(), "a_3")
        self.assertEqual(response.get_made_to(), test_woman.name)
        self.assertEqual(response.get_value(), 1)

    def test_receives_proposal_and_returns_bounce_or_forward(self):
        test_woman = Woman("test_woman")
        test_woman.set_preference_dict({"a_1": 4, "a_2": 3, "a_3": 1, "a_4": 2})
        test_woman.accepted_proposals = [create_accepted_proposal("a_1", test_woman.get_name()),
                                         create_accepted_proposal("a_2", test_woman.get_name()),
                                         create_accepted_proposal("a_3", test_woman.get_name())]

        proposal = Proposal("a_4", test_woman.name)

        response = test_woman.receives_proposal_and_responds(proposal)

        self.assertTrue(isinstance(response, BounceOrForward))
        self.assertTrue(two_lists_are_same(response.get_accepted_proposals(), test_woman.get_accepted_proposals()))
        self.assertEqual(response.get_new_proposal(), proposal)
        self.assertEqual(response.get_requested_by(), test_woman.get_name())


def create_accepted_proposal(man_name, woman_name):
    proposal = Proposal(man_name, woman_name)
    proposal.value = 1
    return proposal


def create_rejected_proposal(man_name, woman_name):
    proposal = Proposal(man_name, woman_name)
    proposal.value = 0
    return proposal


def create_accepted_rejected_proposal(man_name, woman_name):
    proposal = Proposal(man_name, woman_name)
    proposal.value = -1
    return proposal


def create_empty_proposal(man_name, woman_name):
    return Proposal(man_name, woman_name)


def two_lists_are_same(list_1, list_2):
    return len(list(filter(lambda x: x not in list_1, list_2))) == 0
