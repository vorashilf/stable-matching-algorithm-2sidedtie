from unittest import TestCase
from main.domain.man import Man
from main.messageTypes.proposal import Proposal


class TestMan(TestCase):

    def test_propose_to_a_woman(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 1, "b_3": 1})

        proposal = test_man.propose_to_a_woman()

        self.assertEqual(proposal.get_made_by(), "test_man")
        self.assertIn(proposal.get_made_to(), ["b_1", "b_2", "b_3"])
        self.assertEqual(proposal.get_value(), None)

    def test_propose_to_non_rejected_woman(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 1, "b_3": 1})
        test_man.rejected_proposals = ["b_1", "b_3"]

        proposal = test_man.propose_to_a_woman()

        self.assertEqual(proposal.get_made_by(), "test_man")
        self.assertEqual(proposal.get_made_to(), "b_2")
        self.assertEqual(proposal.get_value(), None)

    def test_propose_to_most_desirable_woman(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 2, "b_3": 3})

        proposal = test_man.propose_to_a_woman()

        self.assertEqual(proposal.get_made_by(), "test_man")
        self.assertEqual(proposal.get_made_to(), "b_3")
        self.assertEqual(proposal.get_value(), None)

    def test_propose_to_most_desirable_and_non_rejected_woman(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 2, "b_3": 3})
        test_man.rejected_proposals = ["b_3"]

        proposal = test_man.propose_to_a_woman()

        self.assertEqual(proposal.get_made_by(), "test_man")
        self.assertEqual(proposal.get_made_to(), "b_2")
        self.assertEqual(proposal.get_value(), None)

    def test_receive_rejected_proposal_and_process(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 1, "b_3": 1})
        test_man.rejected_proposals = ["b_1"]
        test_man.available_proposals = 3
        test_man.promotion_level = 0

        proposal = Proposal(test_man, "b_2")
        proposal.value = 0

        test_man.receive_proposal_with_response(proposal)

        self.assertIn("b_2", test_man.get_rejected_proposals())
        self.assertEqual(test_man.available_proposals, 3)
        self.assertEqual(test_man.promotion_level, 0)

    def test_receive_rejected_proposal_and_get_promoted(self):
        test_man = Man("test_man")
        test_man.set_preference_dict({"b_1": 1, "b_2": 1, "b_3": 1})
        test_man.rejected_proposals = ["b_1", "b_2"]
        test_man.available_proposals = 3
        test_man.promotion_level = 0

        proposal = Proposal(test_man, "b_3")
        proposal.value = 0

        test_man.receive_proposal_with_response(proposal)

        self.assertEqual(test_man.promotion_level, 1)
        self.assertEqual(len(test_man.get_rejected_proposals()), 0)
