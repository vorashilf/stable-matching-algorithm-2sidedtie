from main.proposeManager import ProposeManager
from main.inputManagers.returnListOfMan import return_list_of_man
from main.inputManagers.returnListOfWoman import return_list_of_woman


def stable_matching_algorithm(pm):
    while pm.available_man_exist():
        available_man = pm.choose_any_available_man()
        proposal = available_man.propose_to_a_woman()
        pm.send_proposal_from_man_to_woman_and_get_response(proposal)
        print(str(pm.get_result_dictionary()) + "\n")
    return pm.get_result_dictionary()


if __name__ == '__main__':
    list_of_man = return_list_of_man()
    list_of_woman = return_list_of_woman()
    propose_manager = ProposeManager(list_of_man, list_of_woman)
    print(stable_matching_algorithm(propose_manager))

