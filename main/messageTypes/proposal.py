class Proposal:
    def __init__(self, made_by, made_to):
        self.made_by = made_by
        self.made_to = made_to
        self.value = None

    def get_made_to(self):
        return self.made_to

    def get_made_by(self):
        return self.made_by

    def accept(self):
        self.value = 1

    def reject_new_proposal(self):
        self.value = 0

    def reject_existing_proposal(self):
        self.value = -1

    def get_value(self):
        return self.value

    def __eq__(self, other):
        try:
            return self.get_made_to() == other.get_made_to() \
                   and self.get_made_by() == other.get_made_by() \
                   and self.get_value() is other.get_value()
        except AttributeError:
            return NotImplemented

