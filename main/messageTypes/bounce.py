class Bounce:
    def __init__(self, bounce_or_forward):
        self.list_of_proposals = bounce_or_forward.get_list_of_proposals()
        self.requested_by = bounce_or_forward.requested_by
        self.status = None

    def is_successful(self):
        return self.status

    def get_list_of_proposals(self):
        return self.list_of_proposals




