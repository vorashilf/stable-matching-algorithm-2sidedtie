class BounceOrForward:
    def __init__(self, accepted_proposals, new_proposal, woman_name):
        self.accepted_proposals = accepted_proposals
        self.new_proposal = new_proposal
        self.requested_by = woman_name

    def get_accepted_proposals(self):
        return self.accepted_proposals

    def get_new_proposal(self):
        return self.new_proposal

    def get_all_proposals(self):
        return self.accepted_proposals + [self.new_proposal]

    def get_requested_by(self):
        return self.requested_by

    def reject_accepted_proposal_and_accept_new_one_and_return_both(self, accepted_proposal):
        list_of_proposals_to_be_returned = list()
        if accepted_proposal in self.accepted_proposals:
            accepted_proposal.value = -1
            list_of_proposals_to_be_returned.append(accepted_proposal)
            new_proposal_to_be_accepted = self.new_proposal
            new_proposal_to_be_accepted.value = 1
            list_of_proposals_to_be_returned.append(new_proposal_to_be_accepted)
            return list_of_proposals_to_be_returned
        raise Exception("Inputted proposal is not an accepted one")

    def reject_new_proposal_and_return(self):
        new_proposal_to_be_accepted = self.new_proposal
        new_proposal_to_be_accepted.value = 0
        return new_proposal_to_be_accepted

    def is_accepted_proposal(self, proposal):
        return proposal in self.accepted_proposals
