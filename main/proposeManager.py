from main.messageTypes.proposal import Proposal
from main.messageTypes.bounceOrForward import BounceOrForward
from main.messageTypes.bounce import Bounce
from main.messageTypes.forward import Forward
from main.domain.woman import remove_proposal_from_list
from collections import Counter


def get_women_who_has_same_rank(man, current_woman):
    rank_of_current_woman = man.preference_dict[current_woman.get_name()]
    women_who_has_given_rank = [w for w, r in man.preference_dict.items() if r == rank_of_current_woman]
    women_who_has_given_rank.remove(current_woman.get_name())
    return women_who_has_given_rank


class ProposeManager:

    def __init__(self, list_of_man, list_of_woman):
        self.list_of_man = list_of_man
        self.list_of_woman = list_of_woman

    def available_man_exist(self):
        if any(x.is_available() for x in self.list_of_man):
            return True
        return False

    def choose_any_available_man(self):
        return list(filter(lambda man: man.is_available(), self.list_of_man))[0]

    def send_proposal_from_man_to_woman_and_get_response(self, proposal):
        woman = self.get_woman_by_name(proposal.get_made_to())
        response = woman.receives_proposal_and_responds(proposal)
        list_of_proposals = self.check_type_of_response(response, woman)
        self.process_list_of_proposals(list_of_proposals)

    def check_type_of_response(self, response_from_woman, woman):
        if isinstance(response_from_woman, Proposal):
            return [response_from_woman]
        elif isinstance(response_from_woman, BounceOrForward):
            could_bounce = self.bounce_proposal(response_from_woman, woman)
            if len(could_bounce) != 0:
                return could_bounce
            else:
                could_forward = self.forward_proposal(response_from_woman, woman)
                if len(could_forward) != 0:
                    return could_forward
                else:
                    least_desirable_proposals = self.find_least_desirable_proposal_from_list_for_given_woman(
                        response_from_woman, woman)
                    if len(least_desirable_proposals) == 1:
                        if response_from_woman.is_accepted_proposal(least_desirable_proposals[0]):
                            return response_from_woman.reject_accepted_proposal_and_accept_new_one_and_return_both(
                                least_desirable_proposals[0])
                        else:
                            return response_from_woman.reject_new_proposal_and_return()
                    else:
                        return woman.could_not_bounce_or_forward(response_from_woman.get_all_proposals())

    def bounce_proposal(self, response, current_woman):
        list_of_proposals = list()
        proposals_to_be_bounced = response.get_all_proposals()
        for proposal in proposals_to_be_bounced:
            man_who_sent_proposal = self.get_man_by_name(proposal.get_made_by())
            women_who_has_same_rank = get_women_who_has_same_rank(man_who_sent_proposal, current_woman)
            for woman_name in women_who_has_same_rank:
                woman = self.get_woman_by_name(woman_name)
                if len(woman.get_accepted_proposals()) < 3:
                    proposal1 = Proposal(man_who_sent_proposal.name, woman_name)
                    proposal1.value = 1
                    list_of_proposals.append(proposal1)
                    remove_proposal_from_list(proposals_to_be_bounced, proposal)
                    if response.is_accepted_proposal(proposal):
                        list_of_proposals.extend(
                            response.reject_accepted_proposal_and_accept_new_one_and_return_both(proposal))
                        return list_of_proposals
                    else:
                        # list_of_proposals.append(response.reject_new_proposal_and_return())
                        return list_of_proposals
        return list_of_proposals

    def forward_proposal(self, response, current_woman):
        list_of_proposals = list()
        name_of_man_who_made_at_least_two_proposals = self.get_the_man_who_made_at_least_n_proposal(response, 2)
        if name_of_man_who_made_at_least_two_proposals is not None:
            man = self.get_man_by_name(name_of_man_who_made_at_least_two_proposals[0])
            rank_of_current_woman = man.preference_dict[current_woman.name]
            women_who_has_given_rank = [w for w, r in man.preference_dict.items() if r == rank_of_current_woman]
            women_who_has_given_rank.remove(current_woman.name)
            for woman_name in women_who_has_given_rank:
                if woman_name not in map(lambda p: p.madeTo, man.rejected_proposals):
                    proposal1 = Proposal(man.name, woman_name)
                    self.send_proposal_from_man_to_woman_and_get_response(proposal1)

                    proposal2 = Proposal(man.name, current_woman.name)
                    proposal2.value = -1
                    list_of_proposals.append(proposal2)

                    return list_of_proposals
            return None
        else:
            return None

    def get_woman_by_name(self, name):
        return list(filter(lambda w: w.get_name() == name, self.list_of_woman))[0]

    def get_man_by_name(self, name):
        return list(filter(lambda m: m.get_name() == name, self.list_of_man))[0]

    def process_list_of_proposals(self, list_of_proposals):
        for proposal in list_of_proposals:
            man = self.get_man_by_name(proposal.get_made_by())
            woman = self.get_woman_by_name(proposal.get_made_to())
            man.receive_proposal_with_response(proposal)
            woman.receive_processed_proposal(proposal)

    def get_result_dictionary(self):
        result_dict = dict()
        for man in self.list_of_man:
            result_dict[man.get_name()] = man.get_accepted_proposals()

        return result_dict

    def find_least_desirable_proposal_from_list_for_given_woman(self, response_from_woman, woman_name):
        list_of_proposals = response_from_woman.get_all_proposals()
        men_who_made_proposals_and_their_ranks = dict()
        for proposal in list_of_proposals:
            made_by = proposal.get_made_by()
            woman = self.get_woman_by_name(woman_name)
            men_who_made_proposals_and_their_ranks[made_by] = woman.preference_dict[made_by]
        maximum_rank_in_the_dict = min(men_who_made_proposals_and_their_ranks.values())

        least_desirable_men = [man for man in men_who_made_proposals_and_their_ranks if
                               men_who_made_proposals_and_their_ranks[man] == maximum_rank_in_the_dict]

        if len(least_desirable_men) > 1:
            men_and_their_promotion_levels = dict()
            for proposal in list_of_proposals:
                man = self.get_man_by_name(proposal.get_made_by())
                men_and_their_promotion_levels[man.get_name()] = man.get_promotion_level()
            max_promotion_level_in_the_dict = max(men_and_their_promotion_levels.values())
            least_desirable_men = [man for man in men_and_their_promotion_levels if
                                   men_and_their_promotion_levels[man] == max_promotion_level_in_the_dict]
        return list(filter(lambda x: x.get_made_by() in least_desirable_men, list_of_proposals))

    def get_the_man_who_made_at_least_n_proposal(self, response, n):
        man_and_number_of_proposals = Counter([proposal.get_made_by() for proposal in response.get_all_proposals()])
        maximum_number_of_proposals_made_by_same_man = max(man_and_number_of_proposals.values())
        if (maximum_number_of_proposals_made_by_same_man >= n):
            men_who_made_most_of_proposals = [man for man in man_and_number_of_proposals if
                                              man_and_number_of_proposals[
                                                  man] == maximum_number_of_proposals_made_by_same_man]
            return men_who_made_most_of_proposals[0]
        return None
