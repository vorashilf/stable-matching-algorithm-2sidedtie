from main.domain.man import Man


def return_list_of_man():
    list_of_man = list()

    a_1 = Man("a_1")
    a_1.set_preference_dict({"b_1": 1, "b_2": 1, "b_3": 1})
    list_of_man.append(a_1)

    a_2 = Man("a_2")
    a_2.set_preference_dict({"b_1": 1})
    list_of_man.append(a_2)

    a_3 = Man("a_3")
    a_3.set_preference_dict({"b_2": 1, "b_6": 1, "b_7": 1})
    list_of_man.append(a_3)

    a_4 = Man("a_4")
    a_4.set_preference_dict({"b_1": 1, "b_3": 1, "b_4": 1})
    list_of_man.append(a_4)

    a_5 = Man("a_5")
    a_5.set_preference_dict({"b_4": 1, "b_5": 1, "b_7": 1})
    list_of_man.append(a_5)

    a_6 = Man("a_6")
    a_6.set_preference_dict({"b_3": 1})
    list_of_man.append(a_6)

    a_7 = Man("a_7")
    a_7.set_preference_dict({"b_1": 1, "b_3": 1, "b_7": 1})
    list_of_man.append(a_7)

    return list_of_man
