from main.domain.woman import Woman


def return_list_of_woman():
    list_of_woman = list()

    b_1 = Woman("b_1")
    b_1.set_preference_dict({"a_1": 2, "a_2": 1, "a_4": 2, "a_7": 2})
    list_of_woman.append(b_1)

    b_2 = Woman("b_2")
    b_2.set_preference_dict({"a_1": 2, "a_3": 1})
    list_of_woman.append(b_2)

    b_3 = Woman("b_3")
    b_3.set_preference_dict({"a_1": 4, "a_4": 3, "a_6": 1, "a_7": 2})
    list_of_woman.append(b_3)

    b_4 = Woman("b_4")
    b_4.set_preference_dict({"a_4": 2, "a_5": 1})
    list_of_woman.append(b_4)

    b_5 = Woman("b_5")
    b_5.set_preference_dict({"a_5": 1})
    list_of_woman.append(b_5)

    b_6 = Woman("b_6")
    b_6.set_preference_dict({"a_3": 1})
    list_of_woman.append(b_6)

    b_7 = Woman("b_7")
    b_7.set_preference_dict({"a_3": 1, "a_5": 2, "a_7": 3})
    list_of_woman.append(b_7)

    return list_of_woman
