import operator

from main.messageTypes.proposal import Proposal

number_of_allowed_proposals = 3


class Man:
    def __init__(self, name):
        self.name = name
        self.preference_dict = dict()
        self.rejected_proposals = list()
        self.accepted_proposals = list()
        self.promotion_level = 0
        self.available_proposals = number_of_allowed_proposals

    def set_preference_dict(self, preference_dict):
        self.preference_dict = preference_dict

    def propose_to_a_woman(self):
        non_rejected_women = dict()
        for woman in self.preference_dict.keys():
            if woman not in self.rejected_proposals:
                non_rejected_women[woman] = self.preference_dict[woman]

        woman = max(non_rejected_women.items(), key=operator.itemgetter(1))[0]
        return Proposal(self.name, woman)

    def receive_proposal_with_response(self, received_proposal):
        if received_proposal.get_value() == 1:
            self.accepted_proposals.append(received_proposal.get_made_to())
            self.available_proposals -= received_proposal.get_value()
            self.check_if_get_promoted()
        elif received_proposal.get_value() == 0:
            self.rejected_proposals.append(received_proposal.get_made_to())
            self.available_proposals -= received_proposal.get_value()
            self.check_if_get_promoted()
        elif received_proposal.get_value() == -1:
            self.accepted_proposals.remove(received_proposal.get_made_to())
            self.available_proposals -= received_proposal.get_value()
            self.check_if_get_promoted()

    def check_if_get_promoted(self):
        if set(self.rejected_proposals) == set(self.preference_dict.keys()):
            self.promotion_level += 1
            self.rejected_proposals = list()

    def is_available(self):
        if (self.promotion_level < 3) and (self.available_proposals > 0):
            return True
        return False

    def get_accepted_proposals(self):
        return self.accepted_proposals

    def get_rejected_proposals(self):
        return self.rejected_proposals

    def get_name(self):
        return self.name

    def get_promotion_level(self):
        return self.promotion_level

