from main.messageTypes.bounceOrForward import BounceOrForward
from collections import Counter

number_of_allowed_proposals = 3


def find_man_who_made_most_of_the_proposals(list_of_proposals):
    list_of_men_who_made_proposals = list(map(lambda p: p.get_made_by(), list_of_proposals))
    man_and_times_he_proposed = Counter(list_of_men_who_made_proposals)
    return max(man_and_times_he_proposed.items(), key=lambda x: x[1])[0]


def accept_proposal(proposals):
    proposals.value = 1
    return proposals


def remove_proposal_from_list(proposal_list, proposal):
    for index, p in enumerate(proposal_list):
        if p.get_made_to() == proposal.get_made_to() \
                and p.get_made_by() == proposal.get_made_by() \
                and p.get_value() == proposal.get_value():
            del proposal_list[index]
            break
    return proposal_list


class Woman:

    def __init__(self, name):
        self.name = name
        self.preference_dict = dict()
        self.accepted_proposals = list()
        self.rejected_proposals = list()

    def set_preference_dict(self, preference_dict):
        self.preference_dict = preference_dict

    def receives_proposal_and_responds(self, proposal):
        if len(self.accepted_proposals + [proposal]) <= number_of_allowed_proposals:
            proposal.value = 1
            return proposal
        else:
            return BounceOrForward(self.accepted_proposals, proposal, self.name)

    def could_not_bounce_or_forward(self, list_of_proposals):
        man_who_made_most_proposal_name = find_man_who_made_most_of_the_proposals(list_of_proposals)
        proposal_to_be_rejected = list(filter(lambda p: p.get_made_by() == man_who_made_most_proposal_name,
                                              list_of_proposals))[0]
        # there is exactly one proposal in this list which has value None, either that proposal
        # or already accepted proposal will be rejected, following if else serves to that purpose
        if proposal_to_be_rejected.value is None:
            list_of_proposals.remove(proposal_to_be_rejected)
            self.accepted_proposals = list_of_proposals
            proposal_to_be_rejected.value = 0
            list_of_proposals.append(proposal_to_be_rejected)
            return list_of_proposals
        else:
            list_of_proposals.remove(proposal_to_be_rejected)
            list_of_proposals = list(map(lambda p: accept_proposal(p), list_of_proposals))
            self.accepted_proposals = list_of_proposals
            proposal_to_be_rejected.value = -1
            list_of_proposals.append(proposal_to_be_rejected)
            return list_of_proposals


    def get_accepted_proposals(self):
        return self.accepted_proposals

    def get_name(self):
        return self.name

    def receive_processed_proposal(self, proposal):
        if proposal.get_value() == 1:
            self.accepted_proposals.append(proposal)
            if proposal in self.rejected_proposals:
                self.rejected_proposals = remove_proposal_from_list(self.rejected_proposals, proposal)
        elif proposal.get_value() == 0:
            self.rejected_proposals.append(proposal)
        else:
            self.accepted_proposals = remove_proposal_from_list(self.accepted_proposals, proposal)
            self.rejected_proposals.append(proposal)
